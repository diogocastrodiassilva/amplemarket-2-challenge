import React, { useState } from 'react';

import './App.css';

const placeHolder = '(650) 555-1212';

const transformCellToNumber = (str) => {
  return str.replace(/[()]/g, '').replace(/-/g, '').replace(' ', '');
};

const App = () => {
  const [cellString, setCellString] = useState('');

  const onChange = (e) => {
    const { value } = e.target;

    const numberValue = transformCellToNumber(value);

    if (isNaN(numberValue) || numberValue.length > 10) {
      return;
    }

    // [1, 2]
    // [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
    const numberArray = numberValue.split('');
    let finalValue = numberArray;

    // (650 means that the user is deleting
    if (value.includes('(') && !value.includes(')')) {
      finalValue = numberArray.slice(1);
    } else if (numberArray.length > 2) {
      // (650) 965-5137
      // Create the first three numbers
      const [one, two, three] = numberArray;
      finalValue = ['(', one, two, three, ')'];
    }

    // Create the next three numbers
    if (numberArray.length > 3) {
      const followingNumbers = numberArray.slice(3, 6);
      finalValue = [...finalValue, ' ', ...followingNumbers];
    }

    // Create the last four numbers
    if (numberArray.length > 6) {
      finalValue = [...finalValue, '-', ...numberArray.slice(6)];
    }

    setCellString(finalValue.join(''));
  };

  return (
    <div className="App">
      <input placeholder={placeHolder} onChange={onChange} value={cellString} />
    </div>
  );
};

export default App;
