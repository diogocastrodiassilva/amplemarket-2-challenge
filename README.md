# Amplemarket || Code Challenge

Contains the code for the Amplemarket || code challenge.

## Challenge

Build a phone input which format's the number as you type.

**Notes**

- Assume phone numbers will be American - e.g. (650)-965-5137
- Build it using `React.js`
- Shouldn't allow t type numbers wit more than 10 digits
- The country indicator & dropdown are not requirements. If you wish to support it, feel free to do it.

## How to Run Locally

1. Project dependencies

```bash
npm i
```

2. Running the app locally.

```bash
npm start
```

## Technologies Used

### Create React App
A simple way to setup React without having to configurate Webpack.

